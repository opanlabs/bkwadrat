function getcaptcha(){
	 grecaptcha.render('gocaptcha', {
				'sitekey' : '6LdcPCYUAAAAAPxNGPjDHe2drzwFqN4koRQWBxyc'
			});
	
}
$( document ).ready(function() {
    
  //Dropdown plugin data

  var _ddlLoaded = false;
  var indexDDslick=1;
  lang='en';
  page='';
  if(lang=='en'){
	  indexDDslick=0;
  }
  
  if( page==6)
	{
		 setTimeout(function(){
			
			 grecaptcha.render('gocaptcha', {
				'sitekey' : '6LdcPCYUAAAAAPxNGPjDHe2drzwFqN4koRQWBxyc'
			});
			
		 },3500);
	}
    console.log(indexDDslick);

	var ddData = [
		{
            text: "ENG",
            value: 'en',
            selected: false,
            imageSrc: "assets/img/eng.png"
        },
        {
            text: "NL",
            value: 'nl',
            selected: false,
            imageSrc: "assets/img/nl.png"
        }
    ];  
   $('#myDropdown').ddslick({
        data:ddData,
        defaultSelectedIndex:indexDDslick,
		onSelected: function(selectedData){
			 if(_ddlLoaded  === false) {
                    _ddlLoaded = true;
                }
                else
				{
					alert(baseurl+'/language/'+selectedData.selectedData.value);
					window.location =baseurl+'/language/'+selectedData.selectedData.value;
				}
		} 
    });

   $(".group1").colorbox({rel:'group1'});
	$('.play').on('click',function(){
		$('.bungkusvideo').hide();
		$('#iframeyoutube').attr('src',$(this).attr('data-url')).show();
	});
	
	function validcontact() {
		 $('#info-status').removeClass('bg-danger');
		 
		var msgRequired =' can not be empty';
		var msgExtNotValid =' does not match format';
		// var msgExtNotValid =' does not match format';
		var name = $('.name').val();
		var phone = $('.phone').val();
		var email = $('.email').val();
		var subject = $('.subject').val();
	
		var emailFilter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		var phoneFilter = /[^0-9\.]/g;
		var valid=true;
		 $('#info-status').html('');
		var msg ='<div class="alert alert-danger"><ul>';
        
		
		if(name=="")
		{
			msg+='<li>Vul je naam in.</li>';
			
			valid=false;
		}
		if(phone=="")
		{
			msg+='<li>Vul je Phone in.</li>';
			valid=false;
		}
		else
		{
			if(phone.match(phoneFilter))
			  {
					valid =false;
					$('.errorphone').html("<li>Vul een geldig phone in.</li>");
					
			  }
			
		 }
		
		 if(email=="")
		{
			msg+='<li>Vul je e-mailadres in.</li>';
			
			valid=false;
		}
		else
		{
			
			if(!email.match(emailFilter))
			  {
					valid =false;
					// alert('email2');
					msg+='<li>Vul een geldig e-mailadres in.</li>';
			  }
			 
		 }
		if(subject=="")
		{
			msg+='<li> Vul je subject in. </li>';
			
			valid=false;
		}
		if(grecaptcha.getResponse() == "")
		{
			msg+='<li>Verifieer dat u geen robot bent.</li>';
			valid=false;
		}
		if(valid==false)
		{
			 $('#info-status').html(msg+'</ul></div>');
			  $('#info-status').addClass('bg-danger');
			 	return false;
		}
		// return false;
        
    }

    // Make ColorBox responsive
    jQuery.colorbox.settings.maxWidth  = '95%';
    jQuery.colorbox.settings.maxHeight = '95%';

    // ColorBox resize function
    var resizeTimer;
    function resizeColorBox()
    {
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
                if (jQuery('#cboxOverlay').is(':visible')) {
                        jQuery.colorbox.load(true);
                }
        }, 300);
    }

    // Resize ColorBox when resizing window or changing mobile device orientation
    jQuery(window).resize(resizeColorBox);
    window.addEventListener("orientationchange", resizeColorBox, false);
   
});
