<?php

namespace Admin\Http\Controllers;

use Admin\Facades\Module;
use App\Helpers\ContactUploader;
use App\Repositories\Contracts\ImageRepository;
use App\Repositories\Contracts\ProjectRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Prettus\Validator\Exceptions\ValidatorException;

class ProjectController extends Controller
{
  /**
   * @var ProjectRepository
   */
  protected $repository;

  public function __construct(ProjectRepository $repository)
  {
      $this->repository = $repository;
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
      return view('admin::pages.components.project.index');
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
      return view('admin::pages.components.project.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store()
  {
      try {
          Input::merge([
              'sequence' => $this->repository->all()->max('sequence') + 1,
              'posted_on' => Carbon::createFromFormat('d-m-Y', Input::get('posted_on'))
          ]);

          if (Input::hasFile('contact_file')) {
              $cu = new ContactUploader();
              Input::merge(['contact_image' => $cu->uploadContactImage()]);
          }

          $this->repository->create(Input::all());
      } catch (ValidatorException $e) {
          return redirect()->back()->withInput()->withErrors($e->getMessageBag());
      }

      return redirect()->route('cms.project.index');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return Response
   */
  public function edit($id)
  {
      $model = $this->repository->find($id);

      if (!$model->translate(Session::get('locale')->code))
          $model = $this->repository->createTranslation($model, Session::get('locale')->code);

      return view('admin::pages.components.project.edit', [
          'project' => $model,
          'imageCount' => $model->images->count()
      ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int $id
   * @return Response
   */
  public function update($id)
  {
      try {
          if (Input::hasFile('contact_file')) {
              $cu = new ContactUploader();
              Input::merge(['contact_image' => $cu->uploadContactImage()]);
          }
          Input::merge(['posted_on' => Carbon::createFromFormat('d-m-Y', Input::get('posted_on'))]);
          $this->repository->update(Input::all(), $id);
      } catch(ValidatorException $e) {
          return redirect()->back()->withInput()->withErrors($e->getMessageBag());
      }

      return redirect()->route('cms.project.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
      // Load the model
	 
      $model = $this->repository->find($id);

      // Delete the images
      if (count($model->images)) {
          foreach ($model->images as $image) {
              if ($image->deleteFiles())
                  $image->delete();
          }
      }

      // Delete from database
      $this->repository->delete($id);
      // $this->repository->refreshIndex();
      // $this->repository->refreshContacts();
      return Response::json([
          'status'    => 'success',
          'tree'      => null
      ], 200);
  }

  /**
   * Return list with resource data.
   *
   * @return mixed
   */
  public function data()
  {
      return $this->repository->getForDatatable(['active', 'edit', 'delete']);
  }

  /**
   * Update the sequence
   */
  public function reorder()
  {
      $this->repository->reorder(Input::get('order'));
  }

  /**
   * Change the activity status
   *
   * @param $id
   * @param $status
   * @return \Illuminate\Http\RedirectResponse
   */
  public function status($id, $status)
  {
      $this->repository->activate($status, $id);
      return redirect()->route('cms.project.index');
  }

  /**
   * Receive the image list for this resource
   *
   * @param $id
   * @return mixed
   */
  public function images($id)
  {
      return $this->repository->find($id)->images()->orderBy('sequence')->get()->toJson();
  }
}
