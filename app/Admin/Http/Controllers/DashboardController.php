<?php

namespace Admin\Http\Controllers;

use App\Models\Setting;
use App\Repositories\Contracts\NewsRepository;
use App\Repositories\Contracts\ProductRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use LaravelAnalytics;

class DashboardController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    protected $newsRepository;

    /**
     * @var LaravelAnalytics
     */
    protected $analytics;

    /**
     * @var
     */
    protected $statistics;

    public function __construct(ProductRepository $productRepository, NewsRepository $newsRepository)
    {
        $this->productRepository    = $productRepository;
        $this->newsRepository       = $newsRepository;

        $setting = Setting::where('field', '=', 'ga_site_id')->first();
        if ($setting->value) $this->analytics = LaravelAnalytics::setSiteId('ga:' . $setting->value);
    }

    /**
     * Display the dashboard page.
     *
     * @return Response
     */
    public function index()
    {
        $this->statistics['product'] = $this->productRepository->all()->count();
        $this->statistics['news'] = $this->newsRepository->all()->count();

        return view('admin::pages.dashboard.index', [
            'analytics' => $this->analytics,
            'statistics' => $this->statistics
        ]);
    }

    /**
     * Get Google Analytics results
     *
     * @return mixed
     */
    public function getAnalytics()
    {
        $setting = Setting::where('field', '=', 'ga_site_id')->first();

        if ($setting->value) {
            LaravelAnalytics::setSiteId('ga:' . $setting->value);
        } else {
            return Response::json([
                'totalViews' => 0,
                'totalVisitors' => 0,
                'totalSessions' => 0,
                'visitorsAndPageViews' => null
            ], 200);
        }

        $totalViews = 0;
        $totalVisitors = 0;
        $totalSessions = $this->getTotalSessions();
        $visitorsAndPageViews = LaravelAnalytics::getVisitorsAndPageViews(7);

        foreach ($visitorsAndPageViews as $record) {
            $totalViews += $record['pageViews'];
            $totalVisitors += $record['visitors'];
        }

        return Response::json([
            'totalViews' => $totalViews,
            'totalVisitors' => $totalVisitors,
            'totalSessions' => $totalSessions
        ], 200);
    }

    /**
     * Get the total sessions in the last week
     *
     * @return mixed
     */
    private function getTotalSessions()
    {
        $startDate = new Carbon('last week');
        $endDate = Carbon::now();
        $metrics = "ga:sessions";

        $sessions = LaravelAnalytics::performQuery($startDate, $endDate, $metrics, $others = array());

        return $sessions->totalsForAllResults[$metrics];
    }

    /**
     * Get visitors and page views
     */
    public function getVisitorsAndPageViews()
    {
        $data = [
            'visitors' => [],
            'pageViews' => []
        ];
        $visitorsAndPageViews = $this->analytics->getVisitorsAndPageViews(7);

        foreach ($visitorsAndPageViews as $record) {
            $data['visitors'][] = [
                date($record['date']->toDateTimeString()),
                (int) $record['visitors']
            ];

            $data['pageViews'][] = [
                date($record['date']->toDateTimeString()),
                (int) $record['pageViews']
            ];
        }

        return Response::json($data, 200);
    }
}