<div class="navbar">
    <!-- Open side - Naviation on mobile -->
    <a data-toggle="modal" data-target="#aside" class="navbar-item pull-left hidden-lg-up">
        <i class="material-icons">&#xe5d2;</i>
    </a>
    <!-- / -->

    <!-- Page title - Bind to $state's title -->
    <div class="navbar-item pull-left h5" id="pageTitle">@yield('title')</div>

    <!-- navbar right -->
    <ul class="nav navbar-nav pull-right">
        <li class="nav-item dropdown">
            <a class="nav-link text-muted p-x-xs" data-toggle="dropdown">
                <span class="flag-icon flag-icon-{{ Session::get('locale')->flag }} mr-5"></span> {{ Session::get('locale')->translate(Session::get('locale')->code)->name }}
            </a>
            @include('admin::nav.dropdown-language')
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link p-l b-l" href data-toggle="dropdown">
                <span class="avatar w-32">
                    <img src="{{ asset('/admin/assets/images/default_avatar.png') }}" alt="">
                    <i class="on b-white bottom"></i>
                </span>
            </a>
            @include('admin::nav.dropdown-user')
        </li>
    </ul>
    <!-- / navbar right -->
</div>
