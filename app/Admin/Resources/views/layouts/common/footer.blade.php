<div class="p-a text-xs">
    <div class="pull-right text-muted">
        <span class="hidden-xs-down label blue">CMS v1.0.0</span>
    </div>
    <div class="nav">
        <span class="text-muted">&copy; Copyright {{ date("Y") }}</span>
        <a class="nav-link" target="_blank" href="{{ url('http://ceesenco.com') }}">Cees &amp; Co</a>
    </div>
</div>