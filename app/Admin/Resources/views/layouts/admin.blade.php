<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>@yield('title') - CMS</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- for ios 7 style, multi-resolution icon of 152x152 -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
    <link rel="apple-touch-icon" href="{{ asset('/cms/assets/images/logo.png') }}">
    <meta name="apple-mobile-web-app-title" content="CMS">
    <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
    <meta name="mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" sizes="196x196" href="{{ asset('/admin/assets/images/logo.png') }}">

    <!-- style -->
    <link rel="stylesheet" href="{{ asset('/admin/assets/animate.css/animate.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/material-design-icons/material-design-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/bootstrap/dist/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/libs/datatables/datatables.bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/libs/datatables/rowReorder.bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/admin/libs/select2-bootstrap-theme/select2-bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/libs/select2/css/select2.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/flags/css/flag-icon.css') }}" />
    <link rel="stylesheet" href="{{ asset('/admin/libs/jqtree/css/jqtree.css') }}" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/styles/app.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/styles/font.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/assets/styles/custom.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/libs/multiselect/multi-select.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/libs/datepicker/css/bootstrap-datepicker.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/admin/libs/sweetalert/sweetalert.css') }}" type="text/css" />
	<script>
		basedomain = '{{url()}}';
	</script>
</head>
<body>
<div class="app" id="app">

    <!-- aside -->
    <div id="aside" class="app-aside modal fade nav-dropdown">
        <!-- fluid app aside -->
        <div class="left navside dark dk">
            <div class="row-col">
                <div class="navbar no-radius">
                    @include('admin::blocks.navbar-brand')
                </div>
                <div class="row-row">
                    <div class="row-body scrollable hover">
                        <div class="row-inner">
                            <nav class="nav-light">
                                @include('admin::nav.left')
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- / -->

    <!-- content -->
    <div id="content" class="app-content box-shadow-z0" role="main">
        <div class="app-header white box-shadow">
            @include('admin::layouts.common.header')
        </div>
        <div class="app-footer">
            @include('admin::layouts.common.footer')
        </div>
        <div ui-view class="app-body" id="view">
            @yield('content')
        </div>
    </div>
    <!-- / -->

    @include('admin::common.modal-placeholder')
</div>
<!-- jQuery -->
<script src="{{ asset('/admin/libs/jquery/dist/jquery.js') }}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- Bootstrap -->
<script src="{{ asset('/admin/libs/tether/dist/js/tether.min.js') }}"></script>
<script src="{{ asset('/admin/libs/bootstrap/dist/js/bootstrap.js') }}"></script>
<!-- MomentJS -->
<script src="{{ asset('/admin/libs/moment/moment.js') }}"></script>
<!-- Spinner -->
<script src="{{ asset('/admin/libs/spin/dist/spin.min.js') }}"></script>
<script src="{{ asset('/admin/scripts/jquery.spin.js') }}"></script>
<!-- Datatables -->
<script src="{{ asset('/admin/libs/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/admin/libs/datatables/datatables.bootstrap.js') }}"></script>
<script src="{{ asset('/admin/libs/datatables/handlebars.js') }}"></script>
<script src="{{ asset('/admin/libs/datatables/dataTables.rowReorder.min.js') }}"></script>
<!-- CkEditor -->
<script src="{{ asset('/admin/libs/ckeditor/ckeditor.js') }}"></script>
<!-- DropzoneJS -->
<script src="{{ asset('/admin/libs/dropzone/dropzone.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('/admin/libs/select2/js/select2.full.min.js') }}"></script>
<!-- Datepicker -->
<script src="{{ asset('/admin/libs/datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<!-- SweetAlert -->
<script src="{{ asset('/admin/libs/sweetalert/sweetalert.js') }}"></script>
@include('sweet::alert')
<!-- UI-specific JS -->
<script src="{{ asset('/admin/scripts/ui-ajax.js') }}"></script>
<script src="{{ asset('/admin/scripts/ui-nav.js') }}"></script>
<script src="{{ asset('/admin/scripts/ui-tab.js') }}"></script>
<script src="{{ asset('/admin/scripts/ui-images.js') }}"></script>
@yield('javascript')
</body>
</html>
