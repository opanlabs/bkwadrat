@extends('admin::layouts.admin')

@section('title', 'Producten')

@section('content')
    <div class="row-col">
        <div class="col-sm w-lg w-auto-xs light lt bg-auto">
            <div class="p-a">
                <!-- Parent Tree Structure -->
                <div id="parent-tree" data-name="category-tree" data-url="{{ url('cms/product/tree') }}" data-move="{!! url('cms/product/move-node') !!}"></div>
            </div>
        </div>
        <div class="col-sm">
            <div class="p-a pos-rlt">

                <div class="padding">
                    <div class="row m-b">
                        <div class="col-sm-12 m-b-sm">
                            @if (Auth::user()->hasRole('superadmin') || Auth::user()->can('create-product'))
                                <a href="#" class="btn btn-info btn-create">
                                    <i class="fa fa-plus"></i> Nieuw product
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header">
                            <h2>Producten</h2>
                        </div>
                        <div class="table-responsive">
                            <table id="products-table" class="table table-striped b-t b-b" width="100%">
                                <thead>
                                <tr>
                                    <th style="width:10%">Volgorde</th>
                                    <th style="width:60%">Titel</th>
                                    <th style="width:10%"></th>
                                    <th style="width:10%"></th>
                                    <th style="width:10%"></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('/admin/libs/jqtree/js/tree.jquery.js') }}"></script>
    <script src="{{ asset('/admin/scripts/ui-tree.js') }}"></script>
    <script>
        // Create new element
        $(document.body).on('click', '.btn-create', function() {
            if (node = $tree.tree('getSelectedNode')) {
                location.href = '{{ url('cms/product/create') }}/' + node.id;
            }
        });
    </script>
    <script>
        var oTable = $('#products-table').DataTable({
            stateSave: true,
            processing: true,
            serverSide: true,
            rowReorder: {
                dataSrc: 'sequence'
            },
            ajax: {
                url: '{!! url('cms/product/data') !!}',
                type: 'POST',
                data: function(d){
                    var node = $tree.tree('getSelectedNode');
                    d._token = '{!! csrf_token() !!}';
                    d.id = node.id;
                }
            },
            columns: [
                {data: 'sequence', name: 'sequence'},
                {data: 'name', name: 'name'},
                {data: 'active', name: 'active', orderable: false, searchable: false},
                {data: 'edit', name: 'edit', orderable: false, searchable: false},
                {data: 'delete', name: 'delete', orderable: false, searchable: false}
            ],
            language: {
                url: '{{ asset('/admin/localization/nl/datatable.json') }}'
            }
        });

        oTable.on('row-reorder', function (e, diff, edit) {
            var order = [];
            var parent = $tree.tree('getSelectedNode').id;

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                var rowData = oTable.row( diff[i].node ).data();

                order.push([
                    parent,
                    rowData.id,
                    diff[i].newData
                ]);
            }

            $.ajax({
                url: '{!! url('cms/product/reorder') !!}',
                type: 'POST',
                data: {
                    _token: '{!! csrf_token() !!}',
                    order: order
                }
            }).done(function(data) {
                oTable.draw();
            });
        });
    </script>
@endsection