@extends('admin::layouts.admin')

@section('title', 'Projectitem toevoegen')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/project', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Titel') !!}
                            {!! Form::text('title', Input::old('title'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('posted_on', 'Datum') !!}
                            {!! Form::text('posted_on', \Carbon\Carbon::now()->format("d-m-Y"), ['class' => 'datepicker form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('periode', 'Periode') !!}
                            {!! Form::text('periode', Input::old('periode'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Plaats') !!}
                            {!! Form::text('place', Input::old('place'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gm_lat', 'GPS Latitude') !!}
                            {!! Form::text('gm_lat', Input::old('gm_lat'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gm_long', 'GPS Longitude') !!}
                            {!! Form::text('gm_long', Input::old('gm_long'), ['class' => 'form-control']) !!}
                        </div>
                        
                        <div class="form-group">
                            {!! Form::label('status', 'Status') !!}
                            {!! Form::text('status', Input::old('status'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('client', 'Klant') !!}
                            {!! Form::text('client', Input::old('client'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('activity', 'Werkzaamheden') !!}
                            {!! Form::textarea('activity', Input::old('activity'), ['class' => 'ckeditor']) !!}
                        </div>
                        <hr />
                        <div class="form-group">
                            {!! Form::label('intro', 'Intro') !!}
                            {!! Form::textarea('intro', Input::old('intro'), ['class' => 'ckeditor']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('text', 'Tekst') !!}
                            {!! Form::textarea('text', Input::old('text'), ['class' => 'ckeditor']) !!}
                        </div>
                        <hr />
                        <div class="form-group">
                            {!! Form::label('meta_title', 'Meta titel') !!}
                            {!! Form::text('meta_title', Input::old('meta_title'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('meta_description', 'Meta omschrijving') !!}
                            {!! Form::text('meta_description', Input::old('meta_description'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('meta_keywords', 'Meta steekwoorden') !!}
                            {!! Form::text('meta_keywords', Input::old('meta_keywords'), ['class' => 'form-control']) !!}
                        </div>
                        <hr />
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/project') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy'
        });
    </script>
@endsection
