@extends('admin::layouts.admin')

@section('title', 'Projectitem bewerken')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::model($project, ['route' => ['cms.project.update', $project->id], 'method' => 'PUT', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Inhoud</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Titel') !!}
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('slug', 'Link') !!}
                            {!! Form::text('slug', $project->slug, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('posted_on', 'Datum') !!}
                            {!! Form::text('posted_on', \Carbon\Carbon::createFromFormat("Y-m-d H:i:s", $project->posted_on)->format("d-m-Y"), ['class' => 'datepicker form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('client', 'Klant') !!}
                            {!! Form::text('client', $project->client, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('periode', 'Periode') !!}
                            {!! Form::text('periode', $project->periode, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('place', 'Plaats') !!}
                            {!! Form::text('place', Input::old('place'), ['class' => 'form-control']) !!}
                        </div>
                         <div class="form-group">
                            {!! Form::label('gm_lat', 'GPS Latitude') !!}
                            {!! Form::text('gm_lat', Input::old('gm_lat'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('gm_long', 'GPS Longitude') !!}
                            {!! Form::text('gm_long', Input::old('gm_long'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('status', 'Status') !!}
                            {!! Form::text('status', Input::old('status'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('activity', 'Werkzaamheden') !!}
                            {!! Form::textarea('activity', Input::old('activity'), ['class' => 'ckeditor']) !!}
                        </div>
                        <hr/>
                        <div class="form-group">
                            {!! Form::label('intro', 'Intro') !!}
                            {!! Form::textarea('intro', null, ['class' => 'ckeditor']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('text', 'Tekst') !!}
                            {!! Form::textarea('text', null, ['class' => 'ckeditor']) !!}
                        </div>
                        <hr />
                        <div class="form-group">
                            {!! Form::label('meta_title', 'Meta titel') !!}
                            {!! Form::text('meta_title', Input::old('meta_title'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('meta_description', 'Meta omschrijving') !!}
                            {!! Form::text('meta_description', Input::old('meta_description'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('meta_keywords', 'Meta steekwoorden') !!}
                            {!! Form::text('meta_keywords', Input::old('meta_keywords'), ['class' => 'form-control']) !!}
                        </div>
                        <hr />
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/project') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Uploaden</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <!-- Dropzone Error Message -->
                        <div id="dropzone-error" class="alert hide alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>

                        <!-- Dropzone -->
                        <div id="dropzone" class="b-a b-info b-2x b-dashed p-a-md text-center m-b" data-module="projects" data-type="project" data-id="{{ $project->id }}">
                            Klik of sleep uw afbeelding(en) hiernaartoe.
                        </div>

                        <!-- Dropzone Previews -->
                        <div class="table-responsive">
                            <table class="table white b-a" id="previews">
                                <thead>
                                <tr>
                                    <th>Preview</th>
                                    <th width="40%">Naam</th>
                                    <th>Grootte</th>
                                    <th>Voortgang</th>
                                    <th>Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id="template">
                                    <td>
                                        <span class="preview"><img data-dz-thumbnail /></span>
                                    </td>
                                    <td>
                                        <span class="name" data-dz-name></span>
                                    </td>
                                    <td>
                                        <span class="size" data-dz-size></span>
                                    </td>
                                    <td>
                                        <div class="progress progress-sm m-a-0 m-t-xs active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                            <div class="progress-bar info" style="width:0%;" data-dz-uploadprogress></div>
                                        </div>
                                    </td>
                                    <td nowrap>
                                        <button class="btn white btn-sm start">
                                            <span>Start</span>
                                        </button>
                                        <button data-dz-remove class="btn white btn-sm cancel">
                                            <span>Cancel</span>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div id="actions">
                            <div>
                                <p>Upload voortgang:</p>
                                <div id="total-progress" class="progress progress-striped progress-success" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                    <div class="progress-bar info" style="width:0%;" data-dz-uploadprogress></div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-addon btn-info start">
                                <i class="fa fa-cloud-upload"></i> Uploaden
                            </button>
                            <button type="reset" class="btn btn-addon white cancel">
                                <i class="fa fa-ban"></i> Annuleren
                            </button>
                        </div>
                    </div>
                </div>

                <div class="box m-t">
                    <div class="box-header">
                        <h2>Afbeelding(en)</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table white b-a" id="gallery">
                                <thead>
                                <tr>
                                    <th>Thumbnail</th>
                                    <th width="40%">Naam</th>
                                    <th>Grootte</th>
                                    <th>Acties</th>
                                </tr>
                                </thead>
                                <tbody id="sortable-gallery"></tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy'
        });
    </script>
    <script>
        getImages('project', '{{ $project->id }}');

        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);

        // Modify dropzone createEl to use table tr as template
        Dropzone.createElement = function (string) {
            var el = $(string);
            return el[0];
        };

        var myDropzone = new Dropzone("div#dropzone", {
            url: '{{ url('cms/image/upload') }}', // Set the url
            // maxFiles: '{{ (Module::get('projects')['settings']['images']['max_images'] - $imageCount) }}',
			maxFiles: 100,
            maxFilesize: '{{ Module::get('projects')['settings']['images']['max_size'] }}',
            thumbnailWidth: 40,
            thumbnailHeight: 40,
            parallelUploads: 20,
            previewTemplate: previewTemplate,
            autoQueue: false, // Make sure the files aren't queued until manually added
            previewsContainer: "#previews", // Define the container to display the previews
            acceptedFiles: 'image/*',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="{{ asset('/admin/scripts/ui-dropzone.js') }}"></script>
@endsection
