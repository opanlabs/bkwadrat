@extends('admin::layouts.admin')

@section('title', 'Nieuwe banner')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::open(['url' => 'cms/banner']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Vul de gegevens in</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Titel') !!}
                            {!! Form::text('title', Input::old('title'), ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Omschrijving') !!}
                            {!! Form::textarea('description', Input::old('description'), ['class' => 'ckeditor']) !!}
                        </div>
						<div class="form-group">
                            {!! Form::label('slug', 'Link') !!}
                            {!! Form::text('slug', Input::old('slug'), ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/banner') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection