@extends('admin::layouts.admin')

@section('title', 'Gebruiker bewerken')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::model($user, ['route' => ['cms.user.update', $user->id], 'method' => 'PUT']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Wijzig de gegevens</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Voornaam') !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('surname', 'Achternaam') !!}
                            {!! Form::text('surname', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'E-mailadres') !!}
                            {!! Form::text('email', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('company', 'Bedrijf') !!}
                            {!! Form::text('company', null, ['class' => 'form-control']) !!}
                        </div>
                        <hr>
                        <div class="form-group">
                            {!! Form::label('password', 'Wachtwoord') !!}
                            {!! Form::password('password', ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('password_confirmation', 'Herhaal wachtwoord') !!}
                            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            @if ($user->hasRole('superadmin'))
                                {!! Form::hidden('roles[]', $selected[0]) !!}
                            @else
                                <div class="table-responsive">
                                    {!! Form::label('roles', 'Rollen') !!}
                                    {!! Form::select('roles[]', $roles, $selected, ['id' => 'multi-select', 'multiple' => 'multiple']) !!}
                                </div>
                            @endif
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/user') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('/admin/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script>
        $('#multi-select').multiSelect();
    </script>
@endsection