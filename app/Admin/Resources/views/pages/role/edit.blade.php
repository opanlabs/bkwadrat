@extends('admin::layouts.admin')

@section('title', 'Rol bewerken')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::model($role, ['route' => ['cms.role.update', $role->id], 'method' => 'PUT', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Wijzig de gegevens</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Naam') !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('display_name', 'Weergavenaam') !!}
                            {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Omschrijving') !!}
                            {!! Form::text('description', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <div class="table-responsive">
                                {!! Form::label('permissions', 'Rechten') !!}
                                {!! Form::select('permissions[]', $permissions, $selected, ['id' => 'multi-select', 'multiple' => 'multiple']) !!}
                            </div>
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/role') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('/admin/libs/multiselect/jquery.multi-select.js') }}"></script>
    <script>
        $('#multi-select').multiSelect();
    </script>
@endsection