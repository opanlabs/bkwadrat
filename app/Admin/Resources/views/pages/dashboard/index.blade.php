@extends('admin::layouts.admin')

@section('title', 'Dashboard')

@section('content')
    <!-- Quick Buttons -->
    <div class="padding">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">

                    @if ((Auth::user()->hasRole('superadmin') or Auth::user()->can('access-product')) and (Module::isActive('products')))
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <a class="box-color block p-a accent" href="{{ url('cms/product') }}">
                                <div class="pull-left m-r">
                                    <span class="w-40 dker text-center rounded">
                                        <i class="material-icons">shopping_basket</i>
                                    </span>
                                </div>
                                <div class="clear">
                                    <h4 class="m-a-0 text-md">Product toevoegen</h4>
                                    <small class="text-muted">Totaal {{ $statistics['product'] }} producten</small>
                                </div>
                            </a>
                        </div>
                    @endif

                    @if ((Auth::user()->hasRole('superadmin') or Auth::user()->can('access-news')) and (Module::isActive('news')))
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <a class="box-color block p-a warn" href="{{ url('cms/news') }}">
                                <div class="pull-left m-r">
                                    <span class="w-40 dker text-center rounded">
                                        <i class="material-icons">speaker_notes</i>
                                    </span>
                                </div>
                                <div class="clear">
                                    <h4 class="m-a-0 text-md">Nieuwsitem toevoegen</h4>
                                    <small class="text-muted">Totaal {{ $statistics['news'] }} nieuwsitems</small>
                                </div>
                            </a>
                        </div>
                    @endif

                        @if ((Auth::user()->hasRole('superadmin') or Auth::user()->can('access-setting')))
                            <div class="col-sm-6 col-md-4 col-lg-4">
                                <a class="box-color block p-a primary" href="{{ url('cms/setting/default') }}">
                                    <div class="pull-left m-r">
                                        <span class="w-40 dker text-center rounded">
                                            <i class="material-icons">settings</i>
                                        </span>
                                    </div>
                                    <div class="clear">
                                        <h4 class="m-a-0 text-md">Instelling wijzigen</h4>
                                        <small class="text-muted">Persoonlijke instellingen</small>
                                    </div>
                                </a>
                            </div>
                        @endif

                </div>
            </div>
        </div>

    @if ($analytics)
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="box p-a">
                    <div class="pull-left m-r">
            <span class="w-48 rounded green-200">
              <i class="material-icons">&#xe308;</i>
            </span>
                    </div>
                    <div class="clear">
                        <div class="text-muted">Sessies</div>
                        <h4 class="m-a-0 text-md _600"><span id="sessions">0</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4">
                <div class="box p-a">
                    <div class="pull-left m-r">
            <span class="w-48 rounded yellow-200">
              <i class="material-icons">&#xe8d3;</i>
            </span>
                    </div>
                    <div class="clear">
                        <div class="text-muted">Gebruikers</div>
                        <h4 class="m-a-0 text-md _600"><span id="users">0</span></h4>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 col-sm-4">
                <div class="box p-a">
                    <div class="pull-left m-r">
            <span class="w-48 rounded light-blue-200">
              <i class="material-icons">&#xe320;</i>
            </span>
                    </div>
                    <div class="clear">
                        <div class="text-muted">Paginaweergaven</div>
                        <h4 class="m-a-0 text-md _600"><span id="views">0</span></h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-header">
                        <h3>Bezoekers & Paginaweergaven</h3>
                    </div>
                    <div class="box-body">

                        <div id="views-and-visits" class="chart" style="height:300px; width:100%;"></div>

                    </div>
                </div>
            </div>
        </div>
    @endif
    </div>
@endsection

@section('javascript')
    @if ($analytics)
        <script src="http://code.highcharts.com/highcharts.js"></script>
        <script>
            var spinner = new Spinner().spin();
            var sessions = $('#sessions');
            var users = $('#users');
            var views = $('#views');
            var visitorsAndPageViews;

            $(document).ready(function() {
                sessions.spin("small");
                users.spin("small");
                views.spin("small");

                $.ajax({
                    url: '{{ url('cms/api/analytics') }}',
                    type: 'get',
                    success: function (data) {
                        sessions.html(data.totalSessions);
                        users.html(data.totalVisitors);
                        views.html(data.totalViews);
                    },
                    error: function (err) {
                        console.log(err);
                    }
                });
            });
        </script>

        <script>
            $(function() {
                $.getJSON('{!! url('cms/api/analytics/visitors-and-page-views') !!}', function (result) {
                    $("#views-and-visits").highcharts({
                        data: {
                            json: result
                        },
                        title: '',
                        chart: {
                            type: 'area'
                        },
                        colors: ['#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#2b908f', '#f45b5b', '#91e8e1'],
                        xAxis: {
                            type: 'datetime'
                        },
                        yAxis: {
                            title: {
                                text: null
                            }
                        },
                        series: [
                            {
                                name: 'Paginaweergaven',
                                data: result.pageViews
                            },
                            {
                                name: 'Bezoekers',
                                data: result.visitors
                            }
                        ]
                    });
                });
            });
        </script>
    @endif
@endsection