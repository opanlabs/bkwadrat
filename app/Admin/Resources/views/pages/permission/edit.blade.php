@extends('admin::layouts.admin')

@section('title', 'Rechten bewerken')

@section('content')
    <div class="padding">
        <div class="row">
            <div class="col-md-12">

                @include('admin::common.errors')

                {!! Form::model($permission, ['route' => ['cms.permission.update', $permission->id], 'method' => 'PUT', 'role' => 'form']) !!}

                <div class="box">
                    <div class="box-header">
                        <h2>Wijzig de gegevens</h2>
                    </div>
                    <div class="box-divider m-a-0"></div>
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Naam') !!}
                            {!! Form::text('name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('display_name', 'Weergavenaam') !!}
                            {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Omschrijving') !!}
                            {!! Form::text('description', null, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <footer class="dker p-a text-right">
                        <a href="{{ url('cms/permission') }}" class="btn btn-fw danger">Annuleren</a>
                        {!! Form::submit('Opslaan', ['class' => 'btn btn-fw success']) !!}
                    </footer>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection