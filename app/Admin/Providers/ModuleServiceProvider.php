<?php

namespace Admin\Providers;

use Admin\Services\Module;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any admin services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Module', function()
        {
            return new Module($this->app->make('App\\Repositories\\Contracts\\ModuleRepository'));
        });
    }
}