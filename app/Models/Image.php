<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Image extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['path', 'size', 'extension', 'name', 'description', 'label', 'link', 'original_name', 'sequence', 'active'];

    /**
     * Delete the image files
     */
    public function deleteFiles()
    {
        $path   = public_path() . '/uploads/images/';
        $folder = $path . $this->path;

        return File::deleteDirectory($folder);
    }

    /**
     * Get all of the owning imageable models.
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}
