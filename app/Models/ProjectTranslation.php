<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class ProjectTranslation extends Model implements SluggableInterface
{
    use SluggableTrait;

    /**
     * The attribute that needs to get slugged
     *
     * @var array
     */
    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','slug', 'posted_on', 'client',
        'periode','intro','status','place','text','meta_title','meta_description','meta_keywords','activity',
        'gm_lat', 'gm_long',
        'contact_on_home', 'contact_name', 'contact_phone', 'contact_email', 'contact_link', 'contact_image'
    ];
}
