<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;

class Product extends Model implements LogsActivityInterface
{
    use Translatable, LogsActivity;

    /**
     * The attributes that are translated
     *
     * @var array
     */
    public $translatedAttributes = ['name', 'slug', 'intro', 'text', 'meta_title', 'meta_description', 'meta_keywords'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['attribute_set_id', 'name', 'intro', 'text', 'meta_title', 'meta_description', 'meta_keywords', 'price', 'active'];

    /**
     * Get the attribute set that belongs to the product.
     */
    public function attributeSet()
    {
        return $this->belongsTo('App\Models\AttributeSet');
    }

    /**
     * Get all the images.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    /**
     * The categories that belong to the product.
     */
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category')->withPivot('sequence');
    }

    /**
     * The attributes that belong to the product.
     */
    public function attributes()
    {
        return $this->belongsToMany('App\Models\Attribute')->withPivot('value');
    }

    /**
     * Get the related attribute value by code
     *
     * @param $name
     * @return null
     */
    public function getAttrValue($name)
    {
        if ($result = $this->attributes()->where('name', '=', $name)->first()) return $result->pivot->value;

        return null;
    }

    /**
     * Get the message that needs to be logged for the given event.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return "Product({$this->id}) '{$this->name}' was created";
        }

        if ($eventName == 'updated')
        {
            return "Product({$this->id}) '{$this->name}' was updated";
        }

        if ($eventName == 'deleted')
        {
            return "Product({$this->id}) '{$this->name}' was deleted";
        }

        return '';
    }

    /**
     * Filter by attribute value
     *
     * @param $query
     * @param $name
     * @param $value
     * @return mixed
     */
    public function scopeByAttribute($query, $name, $value)
    {
        return $query->whereHas('attributes', function($query) use ($name, $value)
        {
            $query->where('name', '=', $name)->where('attribute_product.value', '=', $value);
        });
    }
}
