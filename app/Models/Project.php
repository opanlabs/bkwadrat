<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;

class Project extends Model implements LogsActivityInterface
{
    use Translatable, LogsActivity;

    public $translatedAttributes = [
        'title',
        'slug',
        'posted_on',
        'client',
        'periode',
        'intro',
        'status',
        'place',
        'text',
        'meta_title', 'meta_description', 'meta_keywords',
        'activity',
        'gm_lat', 'gm_long',
        'contact_on_home', 'contact_name', 'contact_phone', 'contact_email', 'contact_link', 'contact_image'
    ];

    protected $fillable = [
        'title',
        'slug',
        'posted_on',
        'client',
        'periode',
        'meta_title', 'meta_description', 'meta_keywords',
        'text',
        'intro',
        'sequence',
        'active',
        'status',
        'place',
        'activity',
        'gm_lat', 'gm_long',
        'contact_on_home', 'contact_name', 'contact_phone', 'contact_email', 'contact_link', 'contact_image'
    ];
    /**
     * Get all the images.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    /**
     * Get the message that needs to be logged for the given event.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return "Project#({$this->id}) '{$this->title}' was created";
        }

        if ($eventName == 'updated')
        {
            return "Project#({$this->id}) '{$this->title}' was updated";
        }

        if ($eventName == 'deleted')
        {
            return "Project#({$this->id}) '{$this->title}' was deleted";
        }

        return '';
    }
}
