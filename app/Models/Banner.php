<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\LogsActivity;
use Spatie\Activitylog\LogsActivityInterface;

class Banner extends Model implements LogsActivityInterface
{
    use Translatable, LogsActivity;
	
	 /**
     * The attributes that are translated
     *
     * @var array
     */
    public $translatedAttributes = ['title', 'slug', 'description'];
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
		'slug',
        'description',
        'sequence',
        'active'
    ];

    /**
     * Get all the images.
     */
    public function images()
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    /**
     * Get the message that needs to be logged for the given event.
     *
     * @param string $eventName
     *
     * @return string
     */
    public function getActivityDescriptionForEvent($eventName)
    {
        if ($eventName == 'created')
        {
            return "Banner({$this->id}) '{$this->title}' was created";
        }

        if ($eventName == 'updated')
        {
            return "Banner({$this->id}) '{$this->title}' was updated";
        }

        if ($eventName == 'deleted')
        {
            return "Banner({$this->id}) '{$this->title}' was deleted";
        }

        return '';
    }
}
