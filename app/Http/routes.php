<?php

Route::any('/', 'PagesController@home');
Route::any('/home', 'PagesController@home');
Route::any('organisatie', 'PagesController@organisatie');
Route::any('/interieuropbouw', 'PagesController@interieuropbouw');
Route::any('/projecten', 'PagesController@projecten');
Route::any('/projecten/{slug}', 'PagesController@projectenList');
Route::any('/service', 'PagesController@service');
Route::get('language/{code}', 'PagesController@setLocale');
Route::any('/contact', 'PagesController@contact');
Route::post('/contact', 'PagesController@sendContact');
Route::any('{slug}', 'PagesController@defaultPage'); 

