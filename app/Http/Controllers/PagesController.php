<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use App\Repositories\Contracts\BannerRepository;
use App\Repositories\Contracts\PageRepository;
use App\Repositories\Contracts\BlockRepository;
use App\Repositories\Contracts\ReferentiesRepository;
use App\Repositories\Contracts\LanguageRepository;
use App\Repositories\Contracts\ProjectRepository;
use App\Repositories\Criteria\IsActive;
use App\Repositories\Criteria\IsRoot;
use Illuminate\Http\Request;
use Validator;
use Redirect;
// use Illuminate\View\Engines\CompilerEngine;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use DB;

class PagesController extends Controller
{
    /**
     * @var static vars
     */
    protected $settings, $navigation, $banner,$language;

    /**
     * @var repository vars
     */
    protected $pageRepository, $bannerRepository, $projectRepository,$languageRepository;


    public function __construct(PageRepository $pageRepository,
								BlockRepository $blockRepository,
                                BannerRepository $bannerRepository,
								ProjectRepository $projectRepository,
								LanguageRepository $languageRepository)
    {
        $this->settings = Setting::select('field', 'value')->get()->keyBy('field');
        $this->pageRepository = $pageRepository;
        $this->bannerRepository = $bannerRepository;
		 $this->blockRepository =  $blockRepository;
		   $this->languageRepository =  $languageRepository;
		  $this->projectRepository =  $projectRepository;
		   
		if(!Session::has('locale')) {
			$this->language =$this->loadLanguage('nl');
			 // echo"<pre>";print_r($this->language);die;
		 }
		 else{
			 $this->language = Session::get('locale');
		 }
		
        // $this->navigation = $this->loadNavigationItems();
    }

    /**
     * Load the navigation items
     * @return mixed
     */
    public function loadNavigationItems()
    {
        $items = $this->pageRepository->getByCriteria(new IsActive(), new IsRoot());
        return $items;
    }
	
	/**
     * Load the navigation items
     * @return mixed
     */
    public function loadLanguage($code='nl')
    {
		
        $lang = $this->languageRepository->findByField('code', $code)->first();

        app()->setLocale($code);

        if(!Session::has('locale')) {
            Session::put('locale', $lang);
        } else {
            Session::set('locale', $lang);
        }
		return $lang;
    }

    /**
     * Display the home page
     * @return View
     */
    public function home()
    {
		// echo"<pre>";print_r($this->language->code);die;
		$data['language'] = $this->language->code;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		$data['banner']		=$this->bannerRepository->findByField('id',1)->where('active',1);
		if(isset($data['banner'][0]))
		{
			$data['banner'][0]['images']	=$data['banner'][0]->images()->first();
		}
		$data['page']		=$this->pageRepository->findBySlug('home');
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[1,2,3,4,5,6,7,9,10,11])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data['navigation']);die;
		return view('pages.home', $data);
    }
	
	/**
     * Display the home page
     * @return View
     */
    public function organisatie()
    {
		$data['language'] = $this->language->code;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		$data['page']		=$this->pageRepository->findBySlug('organisatie');
		$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[6,7])->where('active',1)->keyBy('id');
		// echo "<pre>";print_r($data);die;
		return view('pages.organisatie', $data);
    }
	
	
	/**
     * Display the interieuropbouw page
     * @return View
     */
    public function interieuropbouw()
    {
		// echo'ssss';die;
		$data['language'] = $this->language->code;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		
		$data['page']		=$this->pageRepository->findBySlug('interieuropbouw');
		$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->keyBy('id')->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[6,7,9])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data->toArray());die;
        return view('pages.interieuropbouw', $data);
       
    }
	/**
     * Display the service page
     * @return View
     */
    public function service()
    {
		// echo'ssss';die;
		$data['language'] = $this->language->code;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		
		$data['page']		=$this->pageRepository->findBySlug('service');
		$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->keyBy('id')->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[6,7,9])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data->toArray());die;
        return view('pages.service', $data);
       
    }
	
	
	/**
     * Display the service page
     * @return View
     */
    public function projecten()
    {
		// echo'ssss';die;
		$data['language'] = $this->language->code;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		
		$data['page']		=$this->pageRepository->findBySlug('projecten');
		$data['projecten']	= $this->projectRepository->all()->keyBy('id')->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[6,7,8,9])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data['projecten']->toArray());die;
        return view('pages.projecten', $data);
       
    }
	
	/**
     * Display the service page
     * @return View
     */
    public function projectenList($slug)
    {
		// echo'ssss';die;
		$data['language'] = $this->language->code;
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		
		$data['page']		=$this->pageRepository->findBySlug('projecten');
		// $data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->keyBy('id')->where('active',1);
		
		$data['projecten']	= $this->projectRepository->findBySlug('projecten/'.$slug);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[6,7,9])->where('active',1)->keyBy('id');
		// echo"<pre>";print_r($data['projecten']->toArray());die;
        return view('pages.projecten2', $data);
       
    }
	
    /**
     * Display a default page
     * @param $slug
     * @return View
     */
    public function defaultPage($slug)
    {
		
		$data['language'] = $this->language->code;
		
		$data['settings']	=$this->settings;
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		
		$data['projecten']	= $this->projectRepository->findBySlug($slug);
		if(count($data['projecten'])>0)
		{
			$data['page']		=$this->pageRepository->findBySlug('projecten');
			
			$data['statistic'] =$this->blockRepository->findWhereIn('id',[6,7,9])->where('active',1)->keyBy('id');
			// echo"<pre>";print_r($data['projecten']->toArray());die;
			return view('pages.projecten2', $data);
		}
		else
		{			
			$data['page']		=$this->pageRepository->findBySlug($slug);
			$data['subPagesNavigation']	= $this->pageRepository->findByField('parent_id',$data['page']['parent_id'])->where('active',1);
			$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->where('active',1);
			$data['statistic'] =$this->blockRepository->findWhereIn('id',[6,7,9])->where('active',1)->keyBy('id');
			// echo"<pre>";print_r($data->toArray());die;
			return view('pages.default', $data);
		}
		
		
		
		
		
    }

    /**
     * Display the contact page
     * @return View
     */
    public function contact()
    {
		// echo'ssss';die;
		$data['language'] = $this->language->code;
		$data['settings']	=$this->settings;
		
		$data['navigation']	=$this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
		foreach($data['navigation'] as $key=>$row)
		{
			if($row['id']==4){
				$data['navigation'][$key]['subpages']	=  $this->pageRepository->findByField('parent_id',$row['id'])->where('active',1);
			}
			else
			{
				$data['navigation'][$key]['subpages']	= array();
			}
		}
		$data['page']		=$this->pageRepository->findBySlug('contact');
		$data['subPages']	= $this->pageRepository->findByField('parent_id',$data['page']['id'])->keyBy('id')->where('active',1);
		$data['statistic'] =$this->blockRepository->findWhereIn('id',[6,7,9,12,13,14,15])->where('active',1)->keyBy('id');
		 return view('pages.contact', $data);
       
    }
	/**
     * Set the current locale
     *
     * @param $code
     * @param LanguageRepository $repository
     * @return \Illuminate\Http\RedirectResponse
     */
    public function setLocale($code)
    {
        // $this->languageRepository = $languageRepository;

        $lang = $this->languageRepository->findByField('code', $code)->first();

        // app()->setLocale($code);

        if(!Session::has('locale')) {
            Session::put('locale', $lang);
        } else {
            Session::set('locale', $lang);
        }
		return Redirect::back();
		// return redirect()->back();
		
    }

    /**
     * Send the contact form
     * @param Requests\SendContactRequest $request
     * @return Response
     */
    public function sendContact(Request $request)
    {
		$validator = Validator::make(Input::all(), 
			array(
				'email'             => 'required|max:50|email',
				'name'         		=> 'required',
				'unberich'   			 => 'required',
				'g-recaptcha-response' => 'required'
				
			),
			array (
				'name.required' => 'Vul je naam in.',
				'email.required' => 'Vul je e-mailadres in.',
				'email.email' => 'Vul een geldig e-mailadres in.',
				'unberich.required' => 'Vul je unberich in.',
				'g-recaptcha-response.required' => 'De reCaptcha is verplicht.',
				
			)
		);
		if($validator->fails()){
			// echo "<pre>";print_r($validator);die;
			return redirect()->back()->withErrors($validator)->withInput(Input::all());;
		} else {
			$config = $this->settings;
			
			$data = [
				'name' => Input::get('name'),
				'email' => Input::get('email'),
				'unberich' => Input::get('unberich'),
				'msg' => Input::get('message'),
				'config' => $config
			];
			

			Mail::send('emails.contact', $data, function($message) use ($config) {
				$message->from($config['email']['value'], $config['company']['value']);
				$message->to($config['email']['value'], $config['company']['value']);
				$message->subject('Contactformulier');
			});

			return redirect()->back()->with('success', 'Uw bericht is verstuurd.');
		}
    }
}
