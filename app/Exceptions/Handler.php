<?php

namespace App\Exceptions;

use App\Models\Page;
use App\Models\Block;
use App\Models\Setting;
use App\Models\Banner;
use App\Repositories\Contracts\BannerRepository;
use App\Repositories\Contracts\PageRepository;
use App\Repositories\Contracts\BlockRepository;
use App\Repositories\Contracts\ReferentiesRepository;
use App\Repositories\Contracts\LanguageRepository;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];
	
	/**
     * @var repository vars
     */
    protected $pageRepository, $bannerRepository, $languageRepository;


    public function __construct(PageRepository $pageRepository,BlockRepository $blockRepository,
                                BannerRepository $bannerRepository,
								LanguageRepository $languageRepository)
    {
        $this->settings = Setting::select('field', 'value')->get()->keyBy('field');
        $this->pageRepository = $pageRepository;
        $this->bannerRepository = $bannerRepository;
		 $this->blockRepository =  $blockRepository;
		   $this->languageRepository =  $languageRepository;
    }
	
	

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

	
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        if ($e instanceof NotFoundHttpException) {
			$data['settings']	=$this->settings;
            $data['navigation']	= $this->pageRepository->findByField(array('active'=>1,'parent_id'=>NULL));
			
			$data['banner']		=$this->bannerRepository->findByField('id',1)->where('active',1);
			if(isset($data['banner'][0]))
			{
				$data['banner'][0]['images']	=$data['banner'][0]->images()->first();
			}
			
			$data['page']		=$this->pageRepository->findBySlug('404');
			
			if(!Session::has('locale')) {
				 $lang = $this->languageRepository->findByField('code', 'nl')->first();
				 app()->setLocale('nl');
			
			 // echo"<pre>";print_r($this->language);die;
			}
			else{
			 $lang = Session::get('locale');
			}
			$data['language'] = $lang->code;
			$data['statistic'] = $this->blockRepository->findWhereIn('id',[6,7,9])->where('active',1)->keyBy('id');
			// echo"<pre>";print_r($data['page']->toArray());die;
			
			return response()->view('errors.404', $data, 404);
        }

        return parent::render($request, $e);
    }
	
	
}
