<?php

namespace App\Repositories\Eloquent;

use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\SortableTrait;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\RoleRepository;
use App\Models\Role;

/**
 * Class RoleRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class RoleRepositoryEloquent extends BaseRepository implements RoleRepository
{
    use DatatalableTrait, SortableTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return RoleValidator
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\RoleValidator";
    }

    /**
     * Items for select options
     *
     * @param  string $data    column to display in the option
     * @param  string $key     column to be used as the value in option
     * @param  string $orderBy column to sort by
     * @param  string $sort    sort direction
     * @return array           array with key value pairs
     */
    public function getForSelect($data, $key = 'id', $orderBy = 'created_at', $sort = 'DECS')
    {
        $this->applyCriteria();
        $this->applyScope();
        $result =  $this->model
            ->orderBy($orderBy, $sort)
            ->lists($data, $key);
        $this->resetModel();
        return $result;
    }
}
