<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\ProjectRepository;
use App\Models\Project;
use App\Validators\ProjectValidator;;
use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\Sluggable;
use App\Repositories\Traits\SortableTrait;
use App\Repositories\Traits\Translatable;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class ProjectRepositoryEloquent
 * @package namespace App\Repositories\Eloquent;
 */
class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository
{
  use DatatalableTrait, SortableTrait, Activatable, CacheableRepository, Translatable, Sluggable;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Project::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
