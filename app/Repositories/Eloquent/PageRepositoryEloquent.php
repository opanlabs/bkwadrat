<?php

namespace App\Repositories\Eloquent;

use App\Models\Page;
use App\Repositories\Contracts\PageRepository;
use App\Repositories\Traits\Activatable;
use App\Repositories\Traits\DatatalableTrait;
use App\Repositories\Traits\Sluggable;
use App\Repositories\Traits\SortableTrait;
use App\Repositories\Traits\Translatable;
use App\Repositories\Traits\Treeable;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

class PageRepositoryEloquent extends BaseRepository implements PageRepository, CacheableInterface
{
    use DatatalableTrait, SortableTrait, Activatable, CacheableRepository, Translatable, Treeable, Sluggable;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Page::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return Validator
     */
    public function validator()
    {
        return "App\\Repositories\\Validators\\PageValidator";
    }
}