<?php

namespace App\Repositories\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class UserValidator extends LaravelValidator
{
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name' => 'required',
            'surname' => 'required',
            'company' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
            'roles' => 'required'
        ],
        ValidatorInterface::RULE_UPDATE => [
            'name' => 'required',
            'surname' => 'required',
            'company' => 'required',
            'email' => 'required|unique:users',
            'password' => 'confirmed',
            'roles' => 'required'
        ]
    ];
}