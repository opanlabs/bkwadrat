<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PageRepository
 *
 * @package namespace App\Repositories\Contracts;
 */
interface PageRepository extends RepositoryInterface
{
    public function reorder($order);

    public function getForDatatable($actions = []);

    public function activate($status, $id);
}