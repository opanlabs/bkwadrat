<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectRepository
 * @package namespace App\Repositories\Contracts;
 */
interface ProjectRepository extends RepositoryInterface
{
  public function reorder($order);

  public function getForDatatable($actions = []);

  public function activate($status, $id);
}
