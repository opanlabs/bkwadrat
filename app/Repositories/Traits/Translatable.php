<?php

namespace App\Repositories\Traits;

use Illuminate\Support\Facades\Config;
use Language;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Validator\Contracts\ValidatorInterface;

trait Translatable
{
    /**
     * Save a new entity in repository
     *
     * @throws ValidatorException
     * @param array $attributes
     * @return mixed
     */
    public function create(array $attributes)
    {
        $this->resetModel();

        if (!is_null($this->validator)) {
            $this->validator->with($attributes)
                ->passesOrFail(ValidatorInterface::RULE_CREATE);
        }

        $model = $this->model->newInstance($attributes);
        $model->save();

        if (in_array(\Dimsav\Translatable\Translatable::class, class_uses($model))) {
            foreach (Language::getSupportedLocales() as $locale) {
                foreach ($model->translatedAttributes as $attr) {
                    if (array_key_exists($attr, $attributes)) $model->translateOrNew($locale['code'])->$attr = $attributes[$attr];
                }
            }
        }

        $model->save();
        $this->resetModel();

        event(new RepositoryEntityCreated($this, $model));

        return $this->parserResult($model);
    }

    /**
     * Create a new translation for the model
     *
     * @param $model
     * @param $locale
     */
    public function createTranslation($model, $locale)
    {
        $lang = \App\Models\Language::find(1);

        foreach ($model->translatedAttributes as $attr) {
            $model->translateOrNew($locale)->$attr = $model->getTranslation($lang->code)->$attr;
        }

        $model->save();
        $this->resetModel();

        return $this->parserResult($model);
    }
}