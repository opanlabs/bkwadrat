<?php

namespace App\Repositories\Traits;


use Illuminate\Support\Facades\Response;

trait Treeable
{
    protected $json;

    /**
     * Get the tree structure from a collection
     *
     * @param bool $json
     * @return array|null
     */
    public function getForTree($json = true)
    {
        $this->applyCriteria();
        $this->applyScope();

        if ($this instanceof Translatable) {
            $this->withTranslations();
        }

        $results = $this->all();
        $this->resetModel();

        $tree = $this->parseAsTree($results, null);

        return $json ? Response::json($tree, 200) : $tree;
    }

    /**
     * Parse array into hierarchical tree structure
     *
     * @param $tree
     * @param null $root
     * @return array|null
     */
    private function parseAsTree($tree, $root = null)
    {
        $return = array();

        // Traverse the tree and search for direct children of the root
        foreach($tree as $child) {
            // A direct child is found
            if($child->parent_id == $root) {
                // Remove item from tree (we don't need to traverse this again)
                $tree = $tree->except($child->id);
                // Append the child into result array and parse its children
                $return[] = array(
                    'label' => $child->title,
                    'id' => $child->id,
                    'children' => $this->parseAsTree($tree, $child->id)
                );
            }
        }

        return empty($return) ? null : $return;
    }
}