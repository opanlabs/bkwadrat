<?php

namespace App\Repositories\Traits;

use Prettus\Repository\Events\RepositoryEntityUpdated;

trait Activatable
{
    /**
     * Update a entity in repository by id
     *
     * @param $status
     * @param $id
     * @return mixed
     */
    public function activate($status, $id)
    {
        $model = $this->model->findOrFail($id);
        $model->active = $status;
        $model->save();

        event(new RepositoryEntityUpdated($this, $model));

        return $this->parserResult($model);
    }
}