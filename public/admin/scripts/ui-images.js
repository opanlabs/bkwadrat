// Get already uploaded images
function getImages(type, id) {
	// console.log(location);
    // var host = location.protocol + '//' + location.host + '/'; // Default host
	var host =basedomain;
    var url = host + '/cms/' + type + '/images/' + id; // Image source
    var del = host + '/cms/image/delete/' + id; // Delete URL
    var output = '';

    $.get(url, function(data) {
        $.each($.parseJSON(data), function(key, value){
            output = output + "" +
                "<tr id='" + value.id + "'>" +
                    "<td>" +

                        "<img style='min-height:40px; height:40px; width:40px;' class='img-responsive' src='" + host + "/uploads/images/" + value.path + "/thumb." + value.extension + "' />" +

                    "</td>" +
                    "<td>" + value.original_name + "</td>" +
                    "<td>" + (value.size/1024/1024).toFixed(2) + " MB</td>" +
                    "<td>" +
                        "<a data-id='" + value.id + "' class='label info m-r-xs img-edit' href='#'>Bewerk</a>" +
                        "<a data-id='" + value.id + "' data-parent='" + id + "' data-type='" + type + "' class='label img-del danger m-r-xs' href='" + del + "'>Verwijder</a>" +
                    "</td>" +
                "</tr>";
        });

        $('#gallery tbody').html(output);
    });
};

$(document.body).on('click', '.img-edit', function(e) {
    e.preventDefault();

    var id = $(this).data('id');
    var host = location.protocol + '//' + location.host + '/'; // Default host
    var url = host + '/cms/image/' + id + '/edit'; // Image source

    $.get(url, function(data) {
        $('#modal-placeholder').modal();

        $('#modal-placeholder').on('shown.bs.modal', function(){
            $('#modal-placeholder .load_modal').html(data);
        });

        $('#modal-placeholder').on('hidden.bs.modal', function(){
            $('#modal-placeholder .load_modal').html('');
        });
    });
});

$(document).on('click', '.img-del', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var parent_id = $(this).data('parent');
    var type = $(this).data('type');
    var url = $(this).attr('href');

    swal({
        title: 'Weet je zeker dat je deze afbeelding wilt verwijderen?',
        text: 'Je kunt het hierna niet meer herstellen',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Nee',
        confirmButtonText: 'Ja',
        closeOnConfirm: true
    }, function() {
        $.ajax({
                url: url,
                type: 'POST',
                data: {
                    id: id
                }
            })
            .done(function(data) {
                swal('Verwijderd!', 'De afbeelding is successvol verwijderd.', 'success');
                myDropzone.options.maxFiles += 1;
                getImages(type, parent_id);
            })
            .error(function(data) {
                swal('Oeps...', 'Er is iets fout gegaan.', 'error');
            });
    });
});


$('#sortable-gallery').sortable({
    items: 'tr',
    helper: fixHelper,
    update: function (event, ui) {
        var host = location.protocol + '//' + location.host + '/'; // Default host
        var url = host + '/cms/image/reorder';

        var data = $('#sortable-gallery').sortable('toArray', { key: "sort" });

        $.ajax({
            url: url,
            type: 'POST',
            data: {
                order: data
            }
        })
        .error(function() {
            swal('Oeps...', 'Er is iets fout gegaan.', 'error');
        });
    }
}).disableSelection();

var fixHelper = function(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
}