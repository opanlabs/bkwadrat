<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = User::create([
            'name' => 'Guus',
            'surname' => 'Portegies',
            'company' => 'Cees & Co',
            'email' => 'info@ceesenco.com',
            'password' => 'W3p00rt!',
            'active' => 1,
            'sequence' => 1,
            'remember_token' => str_random(10)
        ]);

        $superadmin->attachRole(Role::where('name', '=', 'superadmin')->first());
    }
}
