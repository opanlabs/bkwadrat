@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
<?php $images = $page->images()->get()->toArray(); ?>
 <div id="top-wrapper" @if($images!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif>
	<div class="overlay"></div>
	@include('common.header')
	<div class="page-content-wrapper">
		<div class="column-txt">
			<div class="container">
				<div class="column-one">
					<div class="title">{!!$page->translate($language)->intro!!}</div>
					<div class="desc">{!!$page->translate($language)->text!!}</div>
				</div>							
			</div>
		</div>
	</div>
</div>
<div class="border-double">
	<div class="border1"></div>
	<div class="border2"></div>
</div>
<div class="column-content-wrapper">
	<div class="title-wrapper">
		<div class="title">
			<div class="border-top"></div>
			<div class="border-bottom"></div>
			<div class="ico"><img src="{{ asset('/app/img/bg-title.png')}}" alt="" class="img-responsive"></div>
			<div class="block">{!!$page->translate($language)->title!!}</div>								
		</div>
	</div>
	<div class="container">
		<div class="column-desc">
			@if(isset($statistic['8']))
			{!!$statistic['8']->translate($language)['text']!!}
			@endif
		</div>
		<div class="column-list-project">
			@foreach($projecten as $key=>$row)
			<?php $images = $row->images()->get()->toArray(); ?>
			<div class="list-project col-md-4">
				<div class="block">
					<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
					<div class="column">
						<div class="title">{{$row->translate($language)['title']}}</div>
						<div class="desc">{!!$row->translate($language)['intro']!!}</div>
						<div class="btn-more hvr-float-shadow">
							<a href="{!!$row['slug']!!}">
								<span class="txt">@if($language=='nl') Meer Info @else More Info @endif</span>
								<span class="bulet">
									<i class="fa fa-angle-right" aria-hidden="true"></i>
								</span>
							</a>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			
		</div>
	</div>
</div>
@endsection