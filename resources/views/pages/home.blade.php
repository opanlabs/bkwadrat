@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
<div id="home-wrapper" style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $banner[0]['images']['path'] .'/banner.' . $banner[0]['images']['extension']) }}) no-repeat scroll center center / cover !important">
	<div class="overlay"></div>
	@include('common.header')
	<div class="home-content-wrapper">
		<div class="column-txt">
			<div class="container">
				<div class="column-one" >
					<div class="title">{{$banner[0]->translate($language)['title']}}</div>
					<div class="desc">{!!str_limit(strip_tags($banner[0]->translate($language)['description']),120,'')!!}</div>
					@if(isset($statistic['7']) || @$statistic['7']['slug']!='')
						<div class="btn-play hvr-float-shadow">
							<a href="{!!$statistic['7']['slug']!!}">
								<span class="txt">{!!$statistic['7']->translate($language)['text']!!}</span>
								<span class="bulet"><i class="fa fa-angle-down" aria-hidden="true"></i></span>
							</a>
						</div>
					@endif
				</div>							
			</div>
			<div class="column-two">
				<div class="home-column-block">
					@if(isset($statistic['9']))
					<div class="block1">
						<a href="{!!$statistic['9']['slug']!!}">
							<span class="txt">{!!$statistic['9']->translate($language)['title']!!}</span>
							<span class="border">&nbsp;</span>
						</a>
					</div>
					@endif
					@if(isset($statistic['10']))
					<div class="block2">
						<a href="{!!$statistic['10']['slug']!!}">
							<span class="txt">{!!$statistic['10']->translate($language)['title']!!}</span>
							<span class="border">&nbsp;</span>
						</a>
					</div>
					@endif
					@if(isset($statistic['11']))
					<div class="block3">
						<a href="{!!$statistic['11']['slug']!!}">
							<span class="txt">{!!$statistic['11']->translate($language)['title']!!}</span>
							<span class="border">&nbsp;</span>
						</a>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
<div class="border-double">
	<div class="border1"></div>
	<div class="border2"></div>
</div>
<div class="column-content-wrapper">
	<div class="container">
		<div class="video-wrapper">
			<div class="title-wrapper">
				@if(isset($statistic['1']))
				<div class="title">
					<div class="border-top"></div>
					<div class="border-bottom"></div>
					<div class="ico"><img src="{{ asset('/app/img/bg-title.png') }}" alt="" class="img-responsive"></div>
					<div class="block">{{$statistic['1']->translate($language)['title']}}</div>								
				</div>
				@endif
			</div>
			@if(isset($statistic['1']))
			<?php $images = $statistic['1']->images()->get()->toArray(); ?>
			<div class="videos">
				<div class="bungkusvideo">
					<div class="img"><img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive"></div>
					<div class="play" data-url="{!!strip_tags($statistic['1']->translate($language)['text'])!!}">
						<a href="javascript:void(0)" >
							<img src="{{ asset('/app/img/btn-play.png') }}" alt="" class="img-responsive">
						</a>
					</div>
				</div>
				<iframe id="iframeyoutube" width="100%" style="display:none;" height="735" src=""></iframe>
			</div>
			@endif
		</div>
	</div>
	<div class="border-gap"></div>
	<div class="container">
		<div class="home-article-column">
			<div class="row">	
				@if(isset($statistic['2']))
						<?php $images = $statistic['2']->images()->get()->toArray(); ?>
					<div class="col-md-4">
						<div class="home-articl-block">
							<div class="img">
								<a href="{{$statistic['2']['slug']}}">
									<img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive">
									<span class="hover"><img src="{{ asset('/app/img/hover-article.png') }}" alt="" class="img-responsive"></span>
								</a>
							</div>
							<div class="column">
								<div class="title">{!!$statistic['2']->translate($language)['title']!!}</div>
								<div class="desc">{!!str_limit($statistic['2']->translate($language)['text'],130,'')!!} </div>
								<div class="btn-more hvr-float-shadow"><a href="{{$statistic['2']['slug']}}"><span class="txt">@if($language=='nl') Lees Meer @else See More @endif</span> <span class="bulet"><i class="fa fa-angle-right" aria-hidden="true"></i></span></a></div>
								<div class="clear"></div>
							</div>
							<div class="border-double">
								<div class="border1"></div>
								<div class="border2"></div>
							</div>
						</div>
					</div>
				@endif
				@if(isset($statistic['3']))
					<?php $images = $statistic['3']->images()->get()->toArray(); ?>
					<div class="col-md-4">
						<div class="home-articl-block">
							<div class="img">
								<a href="{{$statistic['3']['slug']}}">
									<img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive">
									<span class="hover"><img src="{{ asset('/app/img/hover-article.png') }}" alt="" class="img-responsive"></span>
								</a>
							</div>
							<div class="column">
								<div class="title">				  {!!$statistic['3']->translate($language)['title']!!}
								</div>
								<div class="desc">{!!str_limit($statistic['3']->translate($language)['text'],130,'')!!}
								</div>
								<div class="btn-more hvr-float-shadow">
									<a href="{{$statistic['3']['slug']}}">
										<span class="txt">@if($language=='nl') Lees Meer @else See More @endif</span> 
										<span class="bulet">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</span>
									</a>
								</div>
								<div class="clear"></div>
							</div>
							<div class="border-double">
								<div class="border1"></div>
								<div class="border2"></div>
							</div>
						</div>
					</div>
				@endif
				@if(isset($statistic['4']))
					<?php $images = $statistic['4']->images()->get()->toArray(); ?>
					<div class="col-md-4">
						<div class="home-articl-block">
							<div class="img">
								<a href="{{$statistic['4']['slug']}}">
									<img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive">
									<span class="hover"><img src="{{ asset('/app/img/hover-article.png') }}" alt="" class="img-responsive"></span>
								</a>
							</div>
							<div class="column">
								<div class="title">				  {!!$statistic['4']->translate($language)['title']!!}
								</div>
								<div class="desc">{!!str_limit($statistic['4']->translate($language)['text'],130,'')!!}
								</div>
								<div class="btn-more hvr-float-shadow">
									<a href="{{$statistic['4']['slug']}}">
										<span class="txt">@if($language=='nl') Lees Meer @else See More @endif</span> 
										<span class="bulet">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</span>
									</a>
								</div>
								<div class="clear"></div>
							</div>
							<div class="border-double">
								<div class="border1"></div>
								<div class="border2"></div>
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
		@if(isset($statistic['5']))
			<?php $images = $statistic['5']->images()->get()->toArray(); ?>
		<div class="home-large-article">
			<div class="home-large-article-left">
				<div class="title">{!!$statistic['5']->translate($language)['title']!!}</div>
				<div class="desc">
					{!!str_limit(strip_tags($statistic['5']->translate($language)['text']),800,'')!!}
				</div>
				<div class="btn-more hvr-float-shadow">
					<a href="{!!$statistic['5']['slug']!!}">
						<span class="txt">@if($language=='nl') Lees Meer @else See More @endif</span> 
						<span class="bulet">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
						</span>
					</a>
				</div>
			</div>
			<div class="home-large-article-right">
				<div class="img">
					<img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive">
				</div>
			</div>
			<div class="clear"></div>
			<div class="border-double">
				<div class="border1"></div>
				<div class="border2"></div>
			</div>
		</div>
		@endif
	</div>
</div>

@endsection