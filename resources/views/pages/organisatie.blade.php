@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
<?php $images = $page->images()->get()->toArray(); ?>
 <div id="top-wrapper"  @if($images!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif>
	<div class="overlay"></div>
	@include('common.header')
	
	<div class="page-content-wrapper">
		<div class="column-txt">
			<div class="container">
				<div class="column-one">
					<div class="title">{!!$page->translate($language)->intro!!}</div>
					<div class="desc">{!!$page->translate($language)->text!!}</div>
				</div>							
			</div>
		</div>
	</div>
</div>
<div class="border-double">
	<div class="border1"></div>
	<div class="border2"></div>
</div>
<div class="column-content-wrapper">
	<div class="title-wrapper">
		<div class="title">
			<div class="border-top"></div>
			<div class="border-bottom"></div>
			<div class="ico"><img src="{{ asset('/app/img/bg-title.png') }}" alt="" class="img-responsive"></div>
			<div class="block">{!!$page->translate($language)->title!!}</div>								
		</div>
	</div>
	
	<div class="container">
		@foreach($subPages as $key=>$row)
			<?php $images = $row->images()->get()->toArray(); ?>
			@if($key % 2==0 )
				<div class="page-large-article">
					<div class="page-large-article-left">
						<div class="img">
							@if(isset($images[0]['path']))
							<img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive">
						    @endif
							<div class="overlay"></div>
						</div>
					</div>
					<div class="page-large-article-right">
						<div class="article">
							<div class="title">{{$row->translate($language)['title']}}</div>
							<div class="desc">
								{!!str_limit($row->translate($language)['intro'],800,'')!!}
							</div>
						</div>							
					</div>
					<div class="clear"></div>
					<div class="border-double">
						<div class="border1"></div>
						<div class="border2"></div>
					</div>
				</div>
			
			@else
				<div class="page-large-article">
					<div class="page-large-article-left">
						<div class="article">
							<div class="title">{{$row->translate($language)['title']}}</div>
								<div class="desc">
									{!!str_limit($row->translate($language)['intro'],800,'')!!}
								</div>
						</div>	
					</div>
					<div class="page-large-article-right">
						<div class="img">
							@if(isset($images[0]['path']))
							<img src="{{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}" alt="" class="img-responsive">
						    @endif
							<div class="overlay"></div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="border-double">
						<div class="border1"></div>
						<div class="border2"></div>
					</div>
				</div>
			
			
				
			@endif
		
		@endforeach
		
		
	</div>
</div>
@endsection