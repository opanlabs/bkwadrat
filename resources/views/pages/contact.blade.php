@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
<?php $images = $page->images()->get()->toArray(); ?>
   <div id="top-wrapper"  @if($images!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif>
		<div class="overlay"></div>
			@include('common.header')
			<div class="page-content-wrapper">
				<div class="column-txt">
					<div class="container">
						<div class="column-one">
							<div class="title">{!!$page->translate($language)->intro!!}</div>
							<div class="desc">{!!$page->translate($language)->text!!}</div>
						</div>							
					</div>
				</div>
			</div>
		</div>
		<div class="border-double">
			<div class="border1"></div>
			<div class="border2"></div>
		</div>
		<div class="column-content-wrapper">
			<div class="title-wrapper">
				<div class="title">
					<div class="border-top"></div>
					<div class="border-bottom"></div>
					<div class="ico"><img src="{{ asset('/app/img/bg-title.png') }}" alt="" class="img-responsive"></div>
					<div class="block">{!!$page->translate($language)->title!!}</div>								
				</div>
			</div>
			<div class="container">
				<div class="page-large-article">
					<div class="page-large-article-left">
						<div class="img">
							<img src="{{ asset('/app/img/img-contact.jpg') }}" alt="" class="img-responsive">
							<div class="overlay"></div>
							<div class="content-block">
								<div class="list">
									<ul>
										<li style="background: url(app/img/ico-gedung2.png) no-repeat left top;padding-left: 30px;">
											{{ $settings['address']->value }}
										</li>
										<li style="background: url(app/img/ico-tlp2.png) no-repeat left;padding-left: 30px;">
											{{ $settings['phone']->value }}
										</li>
										<li style="background: url(app/img/ico-hp2.png) no-repeat left;padding-left: 30px;">
											{{ $settings['mobile']->value }}
										</li>
										<li style="background: url(app/img/ico-mail2.png) no-repeat left;padding-left: 30px;"><a href="mailto:$settings['email']->value }}">{{ $settings['email']->value }}</a></li>
										<li style="background: url(app/img/ico-globe2.png) no-repeat left;padding-left: 30px;"><a href="{{$settings['website']->value }}" target="_blank">{{$settings['website']->value }}</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="page-large-article-right">
						<div class="contact">
							<div class="title-column">
								<div class="title-left pull-left">@if(isset($statistic['17'])) {{$statistic['17']->translate($language)['title']}} @else Wilt u meer informatie @endif </div>
								<div class="title-right pull-right"><div class="phone">	{{ $settings['phone']->value }}</div></div>
							</div>
							<div class="form-wrapper">
								<div id="info-status"> @include('common.message')</div>
								<br>
								<form action="{{ url('contact') }}" onsubmit="return validcontact()" method="post" class="contact-form">
								{{ csrf_field() }}
								  <div class="form-group">
								    <input type="text" name="name" class="form-control name" id="name" placeholder="@if(isset($statistic['12'])) {{$statistic['12']->translate($language)['title']}} @else Naam @endif " required>
								  </div>
								  <div class="form-group">
								    <input type="email" name="email" class="form-control email" id="email" placeholder="@if(isset($statistic['13'])) {{$statistic['13']->translate($language)['title']}} @else Email @endif" required>
								  </div>
								  <div class="form-group">
								    <input type="text" name="phone" class="form-control phone" id="phone" placeholder="@if(isset($statistic['14'])) {{$statistic['14']->translate($language)['title']}} @else Telefoonnummer @endif" required>
								  </div>
								  <div class="form-group">
								   	<textarea class="form-control unberich" name="unberich" rows="3" placeholder="@if(isset($statistic['15'])) {{$statistic['15']->translate($language)['title']}} @else Bericht @endif"></textarea>
								  </div>
								  <div class="akhir">
								  	<div class="akhir-left pull-left">
										<div class="captcay" id="gocaptcha"></div>
									</div>
									<div class="akhir-right pull-right">
										<div class="btn-submit hvr-float-shadow">
											<button type="submit" value="Submit">
												<span class="txt">@if(isset($statistic['16'])) {{$statistic['16']->translate($language)['title']}} @else VERZENDEN @endif</span>
												<span class="bulet">
													<i class="fa fa-angle-right" aria-hidden="true"></i>
												</span>
											</button>
										</div>
									</div>
								</div>									  
							</form>
						</div>
					</div>							
				</div>
				<div class="clear"></div>
					<div class="border-double-contact">
						<div class="border1"></div>
						<div class="border2"></div>
					</div>
				</div>
			</div>
			<div class="maps-wrapper">
				<iframe src="https://www.google.com/maps/embed/v1/place?q={{ $settings['gm_lat']->value }},{{ $settings['gm_long']->value }}&key=AIzaSyBEJKMpH6lYi7wUxzwVyslbr4y85Aq7WVw" width="100%" height="466" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	
@endsection