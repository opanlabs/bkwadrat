@extends('layouts.app')

@section('title', $page->meta_title)

@section('content')
<?php $images = $page->images()->get()->toArray(); ?>
 <div id="top-wrapper" @if($images!='') style="background:rgba(0, 0, 0, 0) url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif>
	<div class="overlay"></div>
	@include('common.header')
	<div class="page-content-wrapper">
		<div class="column-txt">
			<div class="container">
				<div class="column-one">
					<div class="title">{!!$page->translate($language)->intro!!}</div>
					<div class="desc">{!!$page->translate($language)->text!!}</div>
				</div>							
			</div>
		</div>
	</div>
</div>
<div class="border-double">
	<div class="border1"></div>
	<div class="border2"></div>
</div>
<div class="column-content-wrapper">
	<div class="title-wrapper">
		<div class="title">
			<div class="border-top"></div>
			<div class="border-bottom"></div>
			<div class="ico"><img src="{{ asset('/app/img/bg-title.png')}}" alt="" class="img-responsive"></div>
			<div class="block">{!!$page->translate($language)->title!!}</div>								
		</div>
	</div>
	<div class="container">
		<div class="column-list-project">
			<?php $images = $projecten->images()->get()->toArray(); ?>
			@foreach($images as $key=>$row)
			<div class="list-project col-md-4">
				<div class="block">
					<div class="img">
						<a href="{{ asset('/uploads/images/'. $row['path'] .'/original.' . $row['extension']) }}" class="group1"><img src="{{ asset('/uploads/images/'. $row['path'] .'/original.' . $row['extension']) }}" alt="" class="img-responsive"></a>
					</div>
				</div>
			</div>
			@endforeach
			
		</div>
		<div class="center">
			<div class="btn-back">
				<a href="projecten">
					<span class="txt">@if($language=='nl') terug @else back @endif </span>
					<span class="bulet">
						<i class="fa fa-angle-left" aria-hidden="true"></i>
					</span>
				</a>
			</div>
		</div>
	</div>
</div>
@endsection