<div id="menu-wrapper">
	<div class="container">
		<div class="top">
			<div class="col-top pull-right">
				<div class="col">{{ $settings['address']->value }}</div>
				<div class="col">
					<i class="fa fa-phone" aria-hidden="true"></i> {{ $settings['phone']->value }}
				</div>
				<div class="col">
					<i class="fa fa-envelope" aria-hidden="true"></i>
					<a href="mailto:{{ $settings['email']->value }}">{{ $settings['email']->value }}</a> 
				</div>
				<div class="col-sosmed">
					<ul>
						@if($settings['facebook']->value!='')
						<li>
							<a href="{{ $settings['facebook']->value }}" target="_blank">
								<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
						</li>
						@endif
						@if($settings['twitter']->value!='')
						<li>
							<a href="{{ $settings['twitter']->value }}" target="_blank">
								<i class="fa fa-twitter" aria-hidden="true"></i>
							</a>
						</li>
						@endif
						@if($settings['youtube']->value!='')
						<li>
							<a href="{{ $settings['youtube']->value }}" target="_blank">
								<i class="fa fa-youtube-play" aria-hidden="true"></i>
							</a>
						</li>
						@endif
						<li class="language-mobile"> 
							<ul>
								<li @if($language=='en') class="active" @endif> <a href="{{url()}}/language/en">ENG</a></li>
								<li @if($language=='nl') class="active" @endif>  <a href="{{url()}}/language/nl">NL</a></li>
							</ul>
						</li> 
					</ul>
				</div>
			</div>							
		</div>
		<div class="main-menu">
			<div class="logo">
				<a href="{{url()}}">
					<img src="{{ asset('/app/img/logo.jpg') }}" alt="" class="img-responsive">
				</a>
			</div>
			<div class="menus">
				<ul>
					@foreach ($navigation as $key=>$row)
					@if($row->id!=14)
					<li><a href="{{$row['slug']}}" @if($page->slug==$row['slug']) class="active" @endif >{{$row->translate($language)['title']}}</a></li>
					@endif
					@endforeach
					
				</ul>
			</div>
			<div class="menumobile">
				<div class="navbar-header">
			        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        </button>
			    </div>
				<div id="navbar" class="">
				    <ul class="nav navbar-nav">
						@foreach ($navigation as $key=>$row)
						@if($row->id!=14)
				        <li>
							<a href="{{$row['slug']}}" @if($page->slug==$row['slug']) class="active" @endif >{{$row->translate($language)['title']}}</a>
						</li>
						@endif
						@endforeach
				    </ul>
				</div><!--/.nav-collapse -->
			</div>
			<div id="myDropdown"></div>
			<div class="clear"></div>
		</div>
	</div>
</div>