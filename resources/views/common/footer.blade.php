<div id="footer">
	<div class="footer-wrapper">
		<div class="border-double">
			<div class="border1"></div>
			<div class="border2"></div>
		</div>
		<div class="footer-column">
			<div class="container">
				<div class="footer-block col-md-5">
					@if(isset($statistic['6']))
					<div class="title">{!!$statistic['6']->translate($language)['title']!!}	</div>
						<div class="desc">	
							{!!$statistic['6']->translate($language)['text']!!}
						</div>
					@endif
				</div>
				<div class="footer-block col-md-2">
					<div class="title">ADRESGEGEVENS</div>
					<div class="list duo">
						<ul>
							<li style="background: url(app/img/ico-gedung.png) no-repeat;padding-left: 20px;">
								{{ $settings['address']->value }}
							</li>
							<li style="background: url(app/img/ico-tlp.png) no-repeat left;padding-left: 20px;">
								{{ $settings['phone']->value }}
							</li>
							<li style="background: url(app/img/ico-hp.png) no-repeat left;padding-left: 20px;">
								{{ $settings['mobile']->value }}
							</li>
							<li style="background: url(app/img/ico-mail.png) no-repeat left;padding-left: 20px;"><a href="mailto:{{ $settings['email']->value }}">{{ $settings['email']->value }}</a></li>
							<li style="background: url(app/img/ico-globe.png) no-repeat left;padding-left: 20px;"><a href="www.b-kwadraat.nl" target="_blank">www.b-kwadraat.nl</a></li>
						</ul>
					</div>
				</div>
				<div class="footer-block col-md-2">
					<div class="title">menu</div>
						<div class="list">
							<ul>
								@foreach ($navigation as $key=>$row)
								@if($row->id!=14)
									<li><a href="{{$row['slug']}}">{{$row->translate($language)['title']}}</a></li>
								@endif
								@endforeach
							</ul>
						</div>
						<div class="second-block">
							<div class="title">volg ons</div>
							<div class="col-sosmed">
								<ul>
									@if($settings['facebook']['value']!='')
										<li><a href="{{$settings['facebook']['value']}}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
									@endif
									@if($settings['twitter']['value']!='')
										<li><a href="{{$settings['twitter']['value']}}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
									@endif
									@if($settings['youtube']['value']!='')
										<li><a href="{{$settings['youtube']['value']}}" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
									@endif
								</ul>
							</div>
						</div>
					</div>
				</div>						
			</div>
			<div class="footer-bottom">
				Copyright 2017 &copy; Voor al uw schilderwerken - B-Kwadraat.nl I Website door <a href="https://ceesenco.com/" target="_blank">Cees & Co</a>  
				<div class="border"></div>
			</div>
	</div>
</div>