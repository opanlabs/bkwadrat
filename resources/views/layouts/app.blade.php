<!DOCTYPE html>
<html lang="{{$language}}">
<head>
	<meta charset="UTF-8">
	<title>B-Kwadraat</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="{{ asset('/app/stylesheets/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/stylesheets/font-awesome.min.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/stylesheets/colorbox.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/stylesheets/hover.css') }}">
	<link rel="stylesheet" href="{{ asset('/app/stylesheets/print.css') }}">
	<link rel="icon" href="{{ asset('/app/img/favicon.ico') }}" type="image/ico">
	<script src="{{ asset('/app/js/jquery-1.10.1.js') }}"></script>
	<script src="{{ asset('/app/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('/app/js/slick.min.js') }}"></script>
	<script src="{{ asset('/app/js/jquery.ddslick.min.js') }}"></script>
	<script src="{{ asset('/app/js/jquery.colorbox-min.js') }}"></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script>
	 var baseurl = '{{url()}}';
	 var lang    ='{{$language}}';
	 var page = '{{$page->id}}';
	</script>
	<script src="{{ asset('/app/js/main.js') }}"></script>
	
</head>
<body itemscope itemtype="http://schema.org/WebPage">
    <!-- Google Analytics -->
    @if (!empty($settings['ga_tracking_id']->value))
        @include('partials.tracking.google-analytics', ['trackingId' => $settings['ga_tracking_id']->value])
    @endif
	
	<div id="wrapper">
	<div class="content-container">
	@yield('content')
    @include('common.footer')
	</div>
	</div>
</body>
</html>