<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Page title -->
    <title>{{ $settings['company']->value }} - @yield('title')</title>

    <!-- Default meta tags -->
    <meta name="description" content="">
    <meta name="keywords" content="">

    <!-- Google+ meta tags -->
    @include('partials.meta.google')

    <!-- Twitter meta tags -->
    @include('partials.meta.twitter')

    <!-- Facebook Open Graph data -->
    @include('partials.meta.facebook')

    <!-- Bootstrap CSS -->
    <link media="all" type="text/css" href="{{ asset('/app/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link media="all" type="text/css" href="{{ asset('/app/css/ie10-viewport-bug-workaround.css') }}" rel="stylesheet">

    <!-- Google Fonts CSS(external) -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>

    <!-- Font Awesome CSS -->
    <link media="all" type="text/css" href="{{ asset('/app/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link media="all" type="text/css" href="{{ asset('/app/css/style.css') }}" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body itemscope itemtype="http://schema.org/WebPage">
    <!-- Google Analytics -->
    @if (!empty($settings['ga_tracking_id']->value))
        @include('partials.tracking.google-analytics', ['trackingId' => $settings['ga_tracking_id']->value])
    @endif

    @include('common.header')

    <section class="content">
        <div class="container">
            @yield('content')
        </div>
    </section>

    @include('common.footer')

    <!-- jQuery JS -->
    <script src="{{ asset('/app/js/jquery.min.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('/app/js/bootstrap.min.js') }}"></script>
    <!-- Custom JS -->
    <script src="{{ asset('/app/js/main.js') }}"></script>
</body>
</html>