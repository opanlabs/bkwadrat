@extends('layouts.app')

@section('title', '404')

@section('content')
<?php $images = $page->images()->get()->toArray(); ?>
<div id="top-wrapper" @if($images!='') style="background:url({{ asset('/uploads/images/'. $images[0]['path'] .'/original.' . $images[0]['extension']) }}) no-repeat scroll center center / cover !important" @endif>
	<div class="overlay"></div>
	@include('common.header')
	 <div class="page-content-wrapper">
					<div class="column-txt">
						<div class="container">
							<div class="column-one">
								<div class="title">{!!$page->translate($language)->intro!!}</div>
								<div class="desc">{!!$page->translate($language)->text!!}</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
			<div class="border-double">
				<div class="border1"></div>
				<div class="border2"></div>
			</div>
			<div class="column-content-wrapper">
				<div class="title-wrapper">
					<div class="title">
						<div class="border-top"></div>
						<div class="border-bottom"></div>
						<div class="ico"><img src="{{ asset('/app/img/bg-title.png') }}" alt="" class="img-responsive"></div>
						<div class="block">{!!$page->translate($language)->meta_title!!}</div>								
					</div>
				</div>
				<div class="container">
						<div class="page-large-article-right">
							<div class="article">
								<div class="desc">
									<center>{!!$page->translate($language)->text!!}<center>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
@endsection